var doc = {
  "pages": [
    {
      "order": 0,
      "objects": [
        {
          "position": {
            "x": 10,
            "y": 10,
            "z": 0
          },
          "type": "circle",
          "color": "#4286f4",
          "angle": 0,
          "size": {
            "width": 100
          },
          "stroke": {}
        },
        {
          "position": {
            "x": 200,
            "y": 200,
            "z": 0
          },
          "type": "rectangle",
          "color": "#b70b33",
          "angle": 20,
          "size": {
            "width": 100,
            "height": 50
          },
          "stroke": {
            "width": 5,
            "color": "#fff"
          }
        }
      ]
    },
    {
      "order": 1,
      "objects": [
        {
          "position": {
            "x": 200,
            "y": 200,
            "z": 0
          },
          "type": "text",
          "color": "#2116bc",
          "angle": 0,
          "message": "Hello world!",
          "font": {
            "family": "Helvetica",
            "size": 60,
            "anchor": "middle",
            "leading": "1.5em",
            "stretch": "normal",
            "style": "normal",
            "variant": "normal",
            "weight": "bold"
          }
        }
      ]
    }
  ],
  "size": {
    "width": 510,
    "height": 600,
    "unit": "px"
  }
}