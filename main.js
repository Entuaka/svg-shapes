var pages = doc["pages"]
var currentDoc = new Document(doc["size"])
pages.forEach( function (page){
    objects = page["objects"]
    page = currentDoc.addPage(objects)
    objects.forEach( function (object){
        object = page.addObject(object);
    })
});

function addPage(){
   page = currentDoc.addPage();
}

function removePage(){
    activePage = currentDoc.getActivePage();
    currentDoc.removePage(activePage);
}

function addElement(event){
    event.preventDefault();

    var elementProperties = {
        "position": {
            "x": 100,
            "y": 50,
            "z": 0
        },
        "color": "#c63217",
        "angle": 0,
        "size": {
            "width": 200,
            "height": 100
        }
    }

    var elementType = this.querySelector('input[name="type"]').value;
    var elementData = new FormData(this);

    for (var [key, value] of elementData.entries()) {
      elementProperties[key] = value
    }

    activePage = currentDoc.getActivePage();
    activePage.addObject(elementProperties);
}

var addElementForms = document.getElementsByClassName("add-element");
for (var i = 0; i < addElementForms.length; i++) {
    addElementForms[i].addEventListener('submit', addElement, false);
}

function removeElement(){
    activePage = currentDoc.getActivePage();
    activePage.removeActiveObject();
}