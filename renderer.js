function Document(size){
    var root = this;
    this.size = size;
    this.pages = [];
    window.addEventListener('scroll', function() {
        visiblePage = root.getVisiblePage();
        if (visiblePage && (!root.activePage || visiblePage.id != root.activePage.id) ){
            root.setActivePage(visiblePage);
        }
    })
    this.getVisiblePage = function(){
        for(var i = 0; i<root.pages.length; i++){
            page = root.pages[i]
            if (page.isVisible()){
                return page;
            }
        }
    },
    this.addPage = function(objects){
        var page = new Page(root.size, objects, root);
        root.pages.push(page)
        page.render()
        return page
    },
    this.setActivePage = function(page){
        if(root.activePage){
            root.activePage.element.classList.remove("selected-page");
        }
        root.activePage = page;
        root.activePage.element.classList.toggle("selected-page");
        return root.activePage;
    },
    this.getActivePage = function(){
        return root.activePage || root.pages[0];
    },
    this.removePage = function(page){
        if(root.pages.length > 1){
            for(var i = 0; i<root.pages.length; i++){
                if(page.id == root.pages[i].id){
                    page.destroy();
                    root.pages.splice(i, 1);
                }
            }
        }
    }
}

function Page(size, objects, parent){
    var root = this;
    this.document = parent;
    this.element = null;
    this.size = size;
    this.order = 0
    this.objects = objects || []

    this.render = function(){
        var width = root.size["width"] + root.size["unit"];
        var height = root.size["height"] + root.size["unit"];
        root.id = "page_" + root.document.pages.length;
        var pages = document.getElementById("pages");
        var page = document.createElement("section");

        page.className = "page";
        page.id = root.id;
        pages.appendChild(page);
        root.element = page;
        root.draw = SVG(root.id).size( "510px", "660px");
    },
    this.addObject = function(properties){
        object = new Object(properties, root);
        root.objects.push(object);
        object.render()
        return object
    },
    this.isVisible = function(){
        var rect = root.element.getBoundingClientRect();
        var elementTop = rect.top;
        var elementBottom = rect.bottom;
        var isVisible = (elementTop >= 0) && (elementBottom <= window.innerHeight);
        return isVisible
    },
    this.destroy = function(){
        root.element.remove();
    },
    this.getActiveObject = function(){
        return root.activeObject;
    },
    this.setActiveObject = function(object){
        root.activeObject = object;
    },
    this.removeActiveObject = function(){
        if(root.activeObject){
            root.activeObject.selectize(false);
            root.activeObject.remove();
        }
    }
}

function Object(properties, parent){
    var root = this;
    this.parent = parent;
    this.page = parent.draw;

    this.type = properties["type"]
    this.position = properties["position"] || {"x": 10, "y": 10, "x2": 100, "y2": 150}
    this.color = properties["color"] || "#fff"
    this.angle = properties["angle"] || 0
    this.size = properties["size"] || {"width": 50, "height": 100}
    this.stroke = properties["stroke"] || {"width": 0, "color": "#fff"};
    this.font = properties["font"] || {};
    this.message = properties["message"] || "";

    this.render = function(){
        switch(root.type) {
            case "circle":
                root.draw = root.page.circle(root.size["width"]).fill(root.color).move(root.position['x'], root.position["y"])
                break;
            case "rectangle":
                root.draw = root.page.rect(root.size["width"], root.size["height"]).fill(root.color).move(root.position["x"], root.position["y"]).rotate(root.angle);
                break;
            case "text":
                root.draw = root.page.text(root.message).move(root.position["x"],root.position["y"]).font({ fill: root.color, family: root.font["family"], size: root.font["size"] })
                break;
            case "square":
                root.draw = root.page.rect(root.size["width"], root.size["width"]).fill(root.color).move(root.position["x"], root.position["y"])
                break;
            case "ellipse":
                root.draw = root.page.ellipse(root.size["width"], root.size["height"]).fill(root.color).move(root.position["x"], root.position["y"])
                break;
            case "line":
                root.draw = root.page.line(root.position["x"],root.position["y"], 100, 300).fill(root.color).stroke({ width: 8, color: "#65db32"})
                break;
            default:
                console.log("Can't render this type", this.type)
        }

        if(root.draw){
            root.draw.dblclick(function(){
                this.selectize(false);
                root.parent.setActiveObject(null);
            })
            root.draw.click(function(){
                this.selectize().selectize({deepSelect: true}).resize();
                root.parent.setActiveObject(root.draw);
            });
            root.draw.draggable({
               minX: 0
              ,minY: 0
              ,maxX: 510
              ,maxY: 660
            });
        }
    }
}
